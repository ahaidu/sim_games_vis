/*********************************************************************
 * Software License Agreement (BSD License)
 *
 *  Copyright (c) 2013, Andrei Haidu, Institute for Artificial Intelligence,
 *  Universität Bremen.
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   * Neither the name of the Institute for Artificial Intelligence,
 *     Universität Bremen, nor the names of its contributors may be
 *     used to endorse or promote products derived from this software
 *     without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/

#include "VisWidget.hh"

using namespace gazebo;
using namespace sim_games_vis;

// Register this plugin with the simulator
GZ_REGISTER_GUI_PLUGIN(VisWidget)

/////////////////////////////////////////////////
VisWidget::VisWidget() : GUIPlugin()
{
    this->stepSize = 0;

    // connect to the database
    this->conn = new mongo::DBClientConnection();
    this->conn->connect("localhost");

    // get scene manager
    this->sceneManager = gui::get_active_camera()->GetScene()->GetManager();

    // set current color to default
    this->currColor = "Red";

    // create the gui part
    VisWidget::CreateWidget();
}

/////////////////////////////////////////////////
VisWidget::~VisWidget()
{
}

//////////////////////////////////////////////////
void VisWidget::Load(sdf::ElementPtr _sdf)
{
    // Get the type of data from the plugin argument
    if(_sdf->HasElement("type"))
    {
        this->storedDataType = _sdf->Get<std::string>("type");
        std::cout << "Stored data type: " << this->storedDataType << std::endl;
    }
    else
    {
        this->storedDataType = "default";
        std::cout << "Stored data type: " << this->storedDataType << std::endl;
    }

    std::cout << "******** VIS GUI PLUGIN LOADED *********" << std::endl;
}

/////////////////////////////////////////////////
void VisWidget::CreateWidget()
{
    // remove initial window loaded with the plugin
    this->resize(0,0);

    // create new external window for visualizing the collections
    this->mainWindow = new QWidget;

    // set name
    this->mainWindow->setWindowTitle(tr("Gazebo: Collection Vis"));

    // set obj name
    this->mainWindow->setObjectName("collectionSelector");

    // Set the frame background and foreground colors
    this->mainWindow->setStyleSheet(
            "QFrame { background-color : rgba(100, 100, 100, 255); color : white; }");


    // Create the main layout for this widget
    QVBoxLayout *main_layout = new QVBoxLayout;

    // Set main layout margins
    main_layout->setContentsMargins(0, 0, 0, 0);


    /* TREE WIDGETS */
    // create frame to keep all the tree widgets together
    QFrame *tree_frame = new QFrame();

    // tree widget layout
    QVBoxLayout *tree_layout = new QVBoxLayout;

    // create a tree widget
    this->treeWidget = new QTreeWidget();
    this->treeWidget->setColumnCount(1);
    this->treeWidget->header()->hide();
    this->treeWidget->setMinimumSize(100, 200);
    this->treeWidget->setFocusPolicy(Qt::NoFocus);
    this->treeWidget->setSelectionMode(QAbstractItemView::ExtendedSelection);
    this->treeWidget->setSelectionBehavior(QAbstractItemView::SelectRows);
    this->treeWidget->setVerticalScrollMode(QAbstractItemView::ScrollPerPixel);

    // add double click signal
    connect(this->treeWidget, SIGNAL(itemDoubleClicked(QTreeWidgetItem *, int)),
            this, SLOT(OnDoubleClickTrajTree(QTreeWidgetItem *, int)));

    // add widget to layout
    tree_layout->addWidget(new QLabel("Trajs:"));
    tree_layout->addWidget(this->treeWidget);
    tree_layout->setContentsMargins(0, 0, 0, 0);


    // create second tree widget
    this->posTreeWidget = new QTreeWidget();
    this->posTreeWidget->setColumnCount(1);
    this->posTreeWidget->header()->hide();
    this->posTreeWidget->setMinimumSize(100, 200);
    this->posTreeWidget->setFocusPolicy(Qt::NoFocus);
    this->posTreeWidget->setSelectionMode(QAbstractItemView::ExtendedSelection);
    this->posTreeWidget->setSelectionBehavior(QAbstractItemView::SelectRows);
    this->posTreeWidget->setVerticalScrollMode(QAbstractItemView::ScrollPerPixel);

    // add double click signal for the second tree widget
    connect(this->posTreeWidget, SIGNAL(itemDoubleClicked(QTreeWidgetItem *, int)),
            this, SLOT(OnDoubleClickPosTree(QTreeWidgetItem *, int)));

    // add widget to layout
    tree_layout->addWidget(new QLabel("Positions:"));
    tree_layout->addWidget(this->posTreeWidget);
    tree_layout->setContentsMargins(0, 0, 0, 0);


    // add layout to the main frame
    tree_frame->setLayout(tree_layout);

    // add the tree frame to the main layout
    main_layout->addWidget(tree_frame);


    // get the collections for the tree widgets
    VisWidget::GetCollections();


    /* BOTTOM WIDGETS */
    // Bottom layout
    QHBoxLayout *bottom_layout = new QHBoxLayout;

    // create a clear all button widget
    QPushButton *clear_all_button = new QPushButton("Clear All");
    connect(clear_all_button, SIGNAL(clicked()),
            this, SLOT(OnClearAll()));

    // create a clear button widget
    QPushButton *clear_button = new QPushButton("Clear");
    connect(clear_button, SIGNAL(clicked()),
            this, SLOT(OnClear()));

    // create a refresh button widget
    QPushButton *refresh_button = new QPushButton("Refresh");
    connect(refresh_button, SIGNAL(clicked()),
            this, SLOT(OnRefresh()));


    // create a color hacked button widget
    QPushButton *color_button = new QPushButton("Color");
    connect(color_button, SIGNAL(clicked()),
            this, SLOT(OnColor()));

    // create a slider for changing the step size
    QSlider *step_slider = new QSlider(Qt::Horizontal, this);
    connect(step_slider, SIGNAL(valueChanged(int)), this, SLOT(OnSlider(int)));

    // add range for the slider (representing ms)
    step_slider->setRange(0, 500);

    // create a label for outputting the step size
    this->stepLabel = new QLabel(QString::number(this->stepSize) + " (ms)");
    this->stepLabel->setFixedWidth(80);

    // add widget to the bottom layout
    bottom_layout->addWidget(step_slider);
    bottom_layout->addWidget(this->stepLabel);
    bottom_layout->addWidget(clear_all_button);
    bottom_layout->addWidget(clear_button);
    bottom_layout->addWidget(refresh_button);
    bottom_layout->addWidget(color_button);

    // add the button to the main layout
    main_layout->addLayout(bottom_layout);

    // Assign the mainlayout to this widget
    this->mainWindow->setLayout(main_layout);

    this->mainWindow->show();
}



//////////////////////////////////////////////////
void VisWidget::LoadTraj(std::string _coll_ns)
{
    // mongodb projection to drop the _id
    mongo::BSONObj projection = BSON("_id" << 0);

    // query the given collection and sort it on the timestamp
    std::unique_ptr<mongo::DBClientCursor> cursor = conn->query(
                _coll_ns, mongo::Query().sort(BSON("timestamp" << 1)), 0, 0, &projection);

    // vis scale
    const math::Vector3 scale = math::Vector3(0.1, 0.1, 0.05);
    //    const math::Vector3 scale = math::Vector3(0.00005, 0.00005, 0.00005);

    // init prev_ts
    double prev_ts = 0;

    // loop through the results to get the trajectory
    while (cursor->more())
    {
        // get the current bo
        mongo::BSONObj curr_doc = cursor->next();

        const double curr_ts = curr_doc.getField("timestamp").Number();

        // add the pose to the traj if the stepsize is larger than the limit
        if(curr_ts - prev_ts >= this->stepSize)
        {
            // set the prev_ts to the current one
            prev_ts = curr_ts;

            // get the current position
            const math::Vector3 curr_pos = math::Vector3(
                    curr_doc.getFieldDotted("pos.x").Number(),
                    curr_doc.getFieldDotted("pos.y").Number(),
                    curr_doc.getFieldDotted("pos.z").Number());

            // get the current orientation
            const math::Quaternion curr_rot = math::Quaternion(
                    curr_doc.getFieldDotted("rot.x").Number(),
                    curr_doc.getFieldDotted("rot.y").Number(),
                    curr_doc.getFieldDotted("rot.z").Number());

            // create the pose
            const math::Pose curr_pose = math::Pose(curr_pos, curr_rot);

            // create the visual
            VisBase* vis = new VisArrow(
                    this->sceneManager, curr_pos, curr_rot, scale, "Green", "Blue");

            this->visTraj[_coll_ns].AddValue(curr_pose, curr_ts, vis);
        }
    }
//	std::cout << "Traj size: " << this->trajectories[_coll_ns].GetVisuals().size() << std::endl;
}

//////////////////////////////////////////////////
void VisWidget::LoadPosTraj(std::string _coll_ns)
{
    // vector of the 3d points of the links
    std::vector<math::Vector3> links_pos;

    // vis scale
    //	const math::Vector3 scale = math::Vector3(0.1, 0.1, 0.05);
    const math::Vector3 scale = math::Vector3(0.0001, 0.0001, 0.0001);

    // init prev_ts
    double prev_ts = 0;

    // mongodb projection to drop the _id
    mongo::BSONObj projection = BSON("_id" << 0);

    // query the given collection and sort it on the timestamp
    std::unique_ptr<mongo::DBClientCursor> cursor = conn->query(
                _coll_ns, mongo::Query().sort(BSON("timestamp" << 1)), 0, 0, &projection);


    // loop through the results to get the trajectory
    while (cursor->more())
    {
        // get the current bo
        mongo::BSONObj curr_doc = cursor->next();

        // vector of bsonelements with the 3d positions
        std::vector<mongo::BSONElement> pos_elem_v = curr_doc.getField("links_pos").Array();

        const double curr_ts = curr_doc.getField("timestamp").Number();

        // add the pose to the traj if the stepsize is larger than the limit
        if(curr_ts - prev_ts >= this->stepSize)
        {
            // set the prev_ts to the current one
            prev_ts = curr_ts;

            // iterate through vector
            for (auto p_it = pos_elem_v.begin(); p_it != pos_elem_v.end(); p_it++)
            {
                // cast iterator to bson object
                mongo::BSONObj b_obj = (*p_it).Obj();

                // curr 3d position
                math::Vector3 curr_pos = math::Vector3(b_obj.getField("x").Double(),
                                                       b_obj.getField("y").Double(),
                                                       b_obj.getField("z").Double());
                // add curr pos to vector
                links_pos.push_back(curr_pos);

                // create the visual
                VisBase* vis = new VisSphere(
                            this->sceneManager, curr_pos, math::Quaternion(), scale, this->currColor);/*, "Green");*/

                // add visual to the postrajetory map
                this->visPosTraj[_coll_ns].AddValue(
                            math::Pose(curr_pos, math::Quaternion()), curr_ts, vis);
            }
        }
    }
}

//////////////////////////////////////////////////
void VisWidget::GetCollections()
{
    // get database names
    std::list<std::string> db_names = this->conn->getDatabaseNames();

    for(std::list<std::string>::const_iterator db_it = db_names.begin();
            db_it != db_names.end(); db_it++)
    {
        // create top item with the db names
        QTreeWidgetItem *db_item = new QTreeWidgetItem(
                static_cast<QTreeWidgetItem*>(0),
                QStringList(QString::fromStdString(*db_it)));

        // create top item with the db names
        QTreeWidgetItem *pos_db_item = new QTreeWidgetItem(
            static_cast<QTreeWidgetItem*>(0),
            QStringList(QString::fromStdString(*db_it)));

        // get the collection ns
        std::list<std::string> collections = this->conn->getCollectionNames(*db_it);

        for(std::list<std::string>::const_iterator coll_it = collections.begin();
                coll_it != collections.end(); coll_it++)
        {
            // return the first doc of the collection to check for metadata
            mongo::BSONObj first_doc = this->conn->findOne(*coll_it, mongo::BSONObj());

            // check if metadata if of traj type
            if(first_doc.getFieldDotted("metadata.type").str() == "trajectory")
            {
                // create child item with the coll namespaces
                QTreeWidgetItem *coll_item = new QTreeWidgetItem(db_item,
                        QStringList(QString::fromStdString(*coll_it)));

                // TODO fix when found right method to check for this
                // redundantly adding the top level db name only if traj collection exists
                this->treeWidget->addTopLevelItem(db_item);

                // add traj collection
                this->treeWidget->addTopLevelItem(coll_item);
            }

            // check if metadata if of traj type
            else if(first_doc.getFieldDotted("metadata.type").str() == "links_pos")
            {
                // create child item with the coll namespaces
                QTreeWidgetItem *coll_item = new QTreeWidgetItem(pos_db_item,
                        QStringList(QString::fromStdString(*coll_it)));

                // TODO fix when found right method to check for this
                // redundantly adding the top level db name only if traj collection exists
                this->posTreeWidget->addTopLevelItem(pos_db_item);

                // add traj collection
                this->posTreeWidget->addTopLevelItem(coll_item);
            }

            // check if metadata if of traj type
            else if(first_doc.getFieldDotted("metadata.type").str() == "links_trajs")
            {
                // create child item with the coll namespaces
                QTreeWidgetItem *coll_item = new QTreeWidgetItem(pos_db_item,
                        QStringList(QString::fromStdString(*coll_it)));

                // TODO fix when found right method to check for this
                // redundantly adding the top level db name only if traj collection exists
                this->posTreeWidget->addTopLevelItem(pos_db_item);

                // add traj collection
                this->posTreeWidget->addTopLevelItem(coll_item);
            }
        }
    }
}

/////////////////////////////////////////////////
void VisWidget::OnRefresh()
{
    // clear the trajectories
    VisWidget::OnClearAll();

    // remove current
    this->treeWidget->clear();

    // remove current
    this->posTreeWidget->clear();

    // get the trajectory collections
    VisWidget::GetCollections();
}

/////////////////////////////////////////////////
void VisWidget::OnClearAll()
{
    this->visTraj.clear();

    this->visPosTraj.clear();
}

/////////////////////////////////////////////////
void VisWidget::OnClear()
{
    // check if current selected is in any of the trajectories, and remove it
    if(this->visTraj.count(this->currSelectionName))
    {
        auto erase_iter = this->visTraj.find(this->currSelectionName);
        this->visTraj.erase(erase_iter);
    }
    if(this->visPosTraj.count(this->currSelectionName))
    {
        auto erase_iter = this->visPosTraj.find(this->currSelectionName);
        this->visPosTraj.erase(erase_iter);
    }
}

/////////////////////////////////////////////////
void VisWidget::OnSlider(int _step_val)
{
    // set stepsize into ms
    this->stepSize = (double) _step_val / 1000;

    // change label text value
    this->stepLabel->setText(QString::number(this->stepSize) + " (ms)");
}

/////////////////////////////////////////////////
void VisWidget::OnColor()
{
    if (this->currColor == "Red")
    {
        this->currColor = "Blue";
        std::cout << "Curr color: " << this->currColor << std::endl;
    }
    else
    {
        this->currColor = "Red";
        std::cout << "Curr color: " << this->currColor << std::endl;
    }
}

/////////////////////////////////////////////////
void VisWidget::OnDoubleClickTrajTree(QTreeWidgetItem *_item, int _column)
{
    // check if selection is leaf (collection)
    if(_item->childCount() == 0)
    {
        // get the selection text
        const std::string _map_key = _item->text(_column).toStdString();

        // save current selectionname
        this->currSelectionName = _map_key;

        // check if trajectory already loaded (exists)
        if(this->visTraj.count(_map_key))
        {
            // traj already exists, toggle visibility
            std::vector<VisBase*> visuals =
            this->visTraj[_map_key].GetVisuals();

            for(std::vector<VisBase*>::iterator it = visuals.begin(); it != visuals.end(); it++)
            {
                (*it)->GetSceneNode()->flipVisibility("true");
            }
        }
        else
        {
            // load trajectory and set it to visible
            VisWidget::LoadTraj(_item->text(_column).toStdString());
        }
    }
}

/////////////////////////////////////////////////
void VisWidget::OnDoubleClickPosTree(QTreeWidgetItem *_item, int _column)
{
    // check if selection is leaf (collection)
    if(_item->childCount() == 0)
    {
        // get the selection text
        const std::string _map_key = _item->text(_column).toStdString();

        // save current selectionname
        this->currSelectionName = _map_key;

        // check if trajectory already loaded
        if(this->visPosTraj.count(_map_key))
        {
            // traj already exists, toggle visibility
            std::vector<VisBase*> visuals =
                    this->visPosTraj[_map_key].GetVisuals();

            for(std::vector<VisBase*>::iterator it = visuals.begin();
                    it != visuals.end(); it++)
            {
                (*it)->GetSceneNode()->flipVisibility("true");
            }
        }
        else
        {
            // load trajectory and set it to visible
            VisWidget::LoadPosTraj(_item->text(_column).toStdString());
        }
    }
}
