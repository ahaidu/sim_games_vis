/*********************************************************************
 * Software License Agreement (BSD License)
 *
 *  Copyright (c) 2013, Andrei Haidu, Institute for Artificial Intelligence,
 *  Universität Bremen.
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   * Neither the name of the Institute for Artificial Intelligence,
 *     Universität Bremen, nor the names of its contributors may be
 *     used to endorse or promote products derived from this software
 *     without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/

#include "VisArrow.hh"

using namespace sim_games_vis;

////////////////////////////////////////////////
VisArrow::VisArrow()
{
    this->sceneNode = NULL;
}

//////////////////////////////////////////////////
VisArrow::VisArrow(Ogre::SceneManager* _sceneManager,
		const gazebo::math::Vector3 _position,
		const gazebo::math::Quaternion _orientation,
		const gazebo::math::Vector3 _scale,
		const std::string _head_color,
		const std::string _shaft_color)
{
    // init the arrow scene node
    this->sceneNode = _sceneManager->getRootSceneNode()->createChildSceneNode();

    // arrow head mesh from the gazebo paths
    Ogre::Entity* head_entity = _sceneManager->createEntity("axis_head");

    // arrow shaft mesh from the gazebo paths
    Ogre::Entity* shaft_entity = _sceneManager->createEntity("axis_shaft");

    // attach objects to the scene nodes
    this->sceneNode->attachObject(shaft_entity);

    // offset node for the arrow head, child of the arrow node
    Ogre::SceneNode* offset_node = this->sceneNode->createChildSceneNode();

    // attach head entity to the offseted node
    offset_node->attachObject(head_entity);

    // offset the node
    offset_node->setPosition(0,0,0.1);

    // set materials of the entities
    head_entity->setMaterialName("Gazebo/" + _head_color);
    shaft_entity->setMaterialName("Gazebo/" + _shaft_color);

    // set positions
    this->SetPosition(_position);

	// set orientation
    this->SetOrientation(_orientation);

    // set the arrow scale
    this->SetScale(_scale);

	// set visible
    this->SetVisible(true);
}

//////////////////////////////////////////////////
VisArrow::~VisArrow()
{
    delete this->sceneNode;
}

//////////////////////////////////////////////////
void VisArrow::SetScale(const gazebo::math::Vector3 _scale)
{
    this->sceneNode->setScale(_scale.x, _scale.y, _scale.z);
}

//////////////////////////////////////////////////
void VisArrow::SetPosition(const gazebo::math::Vector3 _pos)
{
    this->sceneNode->setPosition(_pos.x, _pos.y, _pos.z);
}

//////////////////////////////////////////////////
void VisArrow::SetOrientation(const gazebo::math::Quaternion _quat)
{
    this->sceneNode->setOrientation(_quat.w, _quat.x, _quat.y, _quat.z);
}

//////////////////////////////////////////////////
void VisArrow::SetVisible(const bool _flag)
{
    this->sceneNode->setVisible(_flag);
}

//////////////////////////////////////////////////
Ogre::SceneNode* VisArrow::GetSceneNode()
{
    return this->sceneNode;
}



