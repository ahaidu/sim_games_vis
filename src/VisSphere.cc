/*********************************************************************
 * Software License Agreement (BSD License)
 *
 *  Copyright (c) 2013, Andrei Haidu, Institute for Artificial Intelligence,
 *  Universität Bremen.
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   * Neither the name of the Institute for Artificial Intelligence,
 *     Universität Bremen, nor the names of its contributors may be
 *     used to endorse or promote products derived from this software
 *     without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/

#include "VisSphere.hh"

using namespace sim_games_vis;

////////////////////////////////////////////////
VisSphere::VisSphere()
{
    this->sceneNode = NULL;
}

//////////////////////////////////////////////////
VisSphere::VisSphere(Ogre::SceneManager* _sceneManager,
		const gazebo::math::Vector3 _position,
		const gazebo::math::Quaternion _orientation,
		const gazebo::math::Vector3 _scale,
        const std::string _sphere_color)
{
    // init the arrow scene node
    this->sceneNode = _sceneManager->getRootSceneNode()->createChildSceneNode();

    // arrow head mesh from the gazebo paths
    Ogre::Entity* sphere_entity = _sceneManager->createEntity(Ogre::SceneManager::PT_SPHERE);

    // attach objects to the scene nodes
    this->sceneNode->attachObject(sphere_entity);

    // set materials of the entities
    sphere_entity->setMaterialName("Gazebo/" + _sphere_color);

    // set positions
    this->SetPosition(_position);

	// set orientation
    this->SetOrientation(_orientation);

    // set the arrow scale
    this->SetScale(_scale);

	// set visible
    this->SetVisible(true);
}

//////////////////////////////////////////////////
VisSphere::~VisSphere()
{
    delete this->sceneNode;
}

//////////////////////////////////////////////////
void VisSphere::SetScale(const gazebo::math::Vector3 _scale)
{
    this->sceneNode->setScale(_scale.x, _scale.y, _scale.z);
}

//////////////////////////////////////////////////
void VisSphere::SetPosition(const gazebo::math::Vector3 _pos)
{
    this->sceneNode->setPosition(_pos.x, _pos.y, _pos.z);
}

//////////////////////////////////////////////////
void VisSphere::SetOrientation(const gazebo::math::Quaternion _quat)
{
    this->sceneNode->setOrientation(_quat.w, _quat.x, _quat.y, _quat.z);
}

//////////////////////////////////////////////////
void VisSphere::SetVisible(const bool _flag)
{
    this->sceneNode->setVisible(_flag);
}

//////////////////////////////////////////////////
Ogre::SceneNode* VisSphere::GetSceneNode()
{
    return this->sceneNode;
}



