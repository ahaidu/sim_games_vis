/*********************************************************************
 * Software License Agreement (BSD License)
 *
 *  Copyright (c) 2013, Andrei Haidu, Institute for Artificial Intelligence,
 *  Universität Bremen.
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   * Neither the name of the Institute for Artificial Intelligence,
 *     Universität Bremen, nor the names of its contributors may be
 *     used to endorse or promote products derived from this software
 *     without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/

#ifndef _VIS_WIDGET_HH_
#define _VIS_WIDGET_HH_

#include <gazebo/common/Plugin.hh>
#include <gazebo/gui/GuiPlugin.hh>
#include <gazebo/gui/gui.hh>
#include <gazebo/rendering/rendering.hh>

#include <mongo/client/dbclient.h>

#include "VisTraj.hh"
#include "VisSphere.hh"
#include "VisArrow.hh"


namespace sim_games_vis
{
class GAZEBO_VISIBLE VisWidget : public gazebo::GUIPlugin
{
    Q_OBJECT

    /// \brief Constructor
    public: VisWidget();

    /// \brief Destructor
    public: virtual ~VisWidget();

    /// \brief Load plugin
    public: virtual void Load(sdf::ElementPtr /*_sdf*/);

    /// \brief Create the widget
    public: void CreateWidget();

    /// \brief Load the trajectory form the database
    private: void LoadTraj(std::string _coll_ns);

    /// \brief Load the trajectory form the database
    private: void LoadPosTraj(std::string _coll_ns);

    /// \brief Get the collection list from the database
    private: void GetCollections();

    /// \brief Callback trigged when the refresh button is pressed.
    protected slots: void OnRefresh();

    /// \brief Callback trigged when the clear all button is pressed.
    protected slots: void OnClearAll();

    /// \brief Callback trigged when the clear button is pressed.
    protected slots: void OnClear();

    /// \brief Callback trigged when the color button is pressed.
    protected slots: void OnColor();

    /// \brief Callback trigged when the slider is moved
    protected slots: void OnSlider(int _step_val);

    /// \brief Callback when an item is selected with a double mouse click.
    /// \param[in] _item Pointer to the item selected.
    /// \param[in] _column Column selected.
    private slots: void OnDoubleClickTrajTree(QTreeWidgetItem *_item, int _column);

    /// \brief Callback when an item is selected with a double mouse click.
    /// \param[in] _item Pointer to the item selected.
    /// \param[in] _column Column selected.
    private slots: void OnDoubleClickPosTree(QTreeWidgetItem *_item, int _column);

    /// \brief Main Frame
    private: QWidget *mainWindow;

    /// \brief The tree widget which holds all the trajectories
    private: QTreeWidget *treeWidget;

    /// \brief The tree widget which holds all the positions
    private: QTreeWidget *posTreeWidget;

    /// \brief Label of the current step size (ms)
    private: QLabel *stepLabel;

    /// \brief Connection to the mongo db
    private: mongo::DBClientConnection* conn;

    /// \brief Ogre scene manager.
    private: Ogre::SceneManager *sceneManager;

    /// \brief Database name
    private: std::string dbName;

    /// \brief Db collection name
    private: std::string collName;

    /// \brief Current selected traj
    private: std::string currSelectionName;

    /// \brief step between creating trajectories
    private: double stepSize;

    /// \brief Trajectories
    private: std::map<std::string, VisTraj> visTraj;

    /// \brief Trajectories
    private: std::map<std::string, VisTraj> visPosTraj;

    /// \brief Current color
    private: std::string currColor;
    
    /// \brief Type of stored mongo data (unreal, gazebo etc.)
    private: std::string storedDataType;
};
}
#endif
