# sim_games_vis #

Package for visualizing trajectories from the [knowrob_sim_games](https://bitbucket.org/ahaidu/knowrob_sim_games) package.

### Prerequisites ###

 * [Gazebo](http://gazebosim.org/)

### Build ###
~~~
mkdir build
cd build
cmake ..
make
~~~

### Usage ###

```
gazebo worlds/vis.world --verbose
```